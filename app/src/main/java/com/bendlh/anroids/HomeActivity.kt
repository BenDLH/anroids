package com.bendlh.anroids

import android.content.Intent
import android.view.View
import com.bendlh.anroids.advanced_1.LiveDataActivity
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.base.ViewModel
import com.bendlh.anroids.basic.basic_1.ButtonActivity
import com.bendlh.anroids.basic.basic_2.ListActivity
import com.bendlh.anroids.basic.basic_3.DetailsActivity
import com.bendlh.anroids.basic.basic_4.ExpandableListActivity
import com.bendlh.anroids.basic.retrofit.RetrofitActivity
import com.bendlh.anroids.coroutines.coroutines_1.NonBlockingActivity
import com.bendlh.anroids.coroutines.coroutines_2.RetrofitCoroutineActivity
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 *
 */
class HomeActivity : BaseActivity(), HomeViewModel.Listener {

    override val viewModel = HomeViewModel(this)

    override fun handleIntent(intent: Intent) {}

    override fun openButtonScreen() = launchActivity(ButtonActivity.getIntent(this))

    override fun openListScreen() = launchActivity(ListActivity.getIntent(this))

    override fun openDetailsScreen() = launchActivity(DetailsActivity.getIntent(this, "Passed in message"))

    override fun openExpandableListScreen() = launchActivity(ExpandableListActivity.getIntent(this))

    override fun openRetrofitScreen() = launchActivity(RetrofitActivity.getIntent(this))

    override fun openLiveDataScreen() = launchActivity(LiveDataActivity.getIntent(this))

    override fun openNonBlockingScreen() = launchActivity(NonBlockingActivity.getIntent(this))

    override fun openRetrofitCoroutinesScreen() = launchActivity(RetrofitCoroutineActivity.getIntent(this))
}

class HomeViewModel(listener: Listener) : BaseViewModel() {

    interface Listener {
        fun openButtonScreen()
        fun openListScreen()
        fun openDetailsScreen()
        fun openExpandableListScreen()
        fun openRetrofitCoroutinesScreen()
        fun openLiveDataScreen()
        fun openNonBlockingScreen()
        fun openRetrofitScreen()
    }

    val itemBinding = ItemBinding.of<Any> { itemBinding, position, item ->
        when (item) {
            is MenuItemViewModel -> itemBinding.set(BR.viewModel, R.layout.item_menu)
            is HeaderItemViewModel -> itemBinding.set(BR.viewModel, R.layout.item_header)
        }
    }
    val items = mutableListOf(
            MenuItemViewModel("Just A Button", listener::openButtonScreen),
            MenuItemViewModel("Simple List", listener::openListScreen),
            MenuItemViewModel("Child ViewModel", listener::openDetailsScreen),
            MenuItemViewModel("Expandable List", listener::openExpandableListScreen),
            MenuItemViewModel("Retrofit", listener::openRetrofitScreen),
            HeaderItemViewModel("Advanced"),
            MenuItemViewModel("Live Data", listener::openLiveDataScreen),
            MenuItemViewModel("Non Blocking Coroutines", listener::openNonBlockingScreen),
            MenuItemViewModel("Retrofit Coroutines", listener::openRetrofitCoroutinesScreen)
    )

    override val bindingLayoutRes = R.layout.view_home
}

class MenuItemViewModel(
        val title: String,
        val onClick: () -> Unit
) {
    fun onClicked(view: View) = onClick()
}

class HeaderItemViewModel(val title: String)