package com.bendlh.anroids.coroutines.coroutines_2

import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import co.metalab.asyncawait.RetrofitHttpError
import co.metalab.asyncawait.async
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.utils.Ticker
import com.bendlh.anroids.utils.VantageServiceManager
import com.bendlh.anroids.utils.log
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import retrofit2.Call

/**
 *
 */
class RetrofitCoroutineActivity : BaseActivity() {

    override val viewModel = RetrofitViewModel(RetrofitDataProvider())

    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, RetrofitCoroutineActivity::class.java)
    }
}

class RetrofitViewModel(private val dataProvider: RetrofitDataProvider) : BaseViewModel() {

    val tickerPrice = ObservableField<String>()
    val tickerVolume = ObservableField<String>()
    val tickerTime = ObservableField<String>()

    private var stopped = false

    override val bindingLayoutRes = R.layout.view_retrofit

    override fun onStart() {
        super.onStart()

        getTicker()
    }

    override fun onStop() {
        super.onStop()

        stopped = true
        async.cancelAll()
    }

    private fun getTicker() {

        launch(CommonPool) {

            val response = dataProvider.getProductTicker()

            // TODO Error handling? Try catch

            tickerPrice.set("" + response.price)
            tickerVolume.set("" + response.volume)
            tickerTime.set("" + response.time)
        }
    }

    private fun getTickerParallel() {

        val one = async(CommonPool) {
            dataProvider.getProductTicker()
        }
        val two = async(CommonPool) {
            dataProvider.getProductTicker()
        }

        launch(CommonPool) {
            val combined = one.await() to two.await()
            println("Kotlin Combined : " + combined)

            // Do something with the data
        }
    }

    private fun getTickerAsyncAwait() {

        async {

            val response = await { dataProvider.getProductTicker() }

            tickerPrice.set("" + response.price)
            tickerVolume.set("" + response.volume)
            tickerTime.set("" + response.time)

        }.onError {
            log(it.toString())
        }
    }
}

class RetrofitDataProvider {

    private val vantageService = VantageServiceManager.service

    fun getProductTicker(): Ticker {
        return successOrThrow(vantageService.getProductTicker("BCH-BTC"))
    }

    private fun <T> successOrThrow(call: Call<T>): T {
        val response = call.execute()
        return if (response.isSuccessful) {
            response.body()
        } else {
            throw RetrofitHttpError(response)
        }
    }
}
