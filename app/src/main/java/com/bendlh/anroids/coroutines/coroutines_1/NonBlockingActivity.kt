package com.bendlh.anroids.coroutines.coroutines_1

import android.content.Context
import android.content.Intent
import android.databinding.BindingAdapter
import android.databinding.ObservableField
import android.view.View
import android.widget.TextView
import co.metalab.asyncawait.async
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.coroutines.coroutines_2.RetrofitCoroutineActivity
import com.bendlh.anroids.utils.toast

/**
 *
 */
class NonBlockingActivity : BaseActivity() {

    override val viewModel = NonBlockingViewModel {
        launchActivity(RetrofitCoroutineActivity.getIntent(this))
    }

    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, NonBlockingActivity::class.java)
    }
}

class NonBlockingViewModel(private val onClicked: () -> Unit) : BaseViewModel() {

    val contents = ObservableField(0)

    override val bindingLayoutRes = R.layout.view_non_blocking

    override fun onStart() {
        super.onStart()
        async {
            await { doSlowStuff() }
        }
    }

    override fun onStop() {
        super.onStop()
        async.cancelAll()
    }

    fun proveNotBlocking(view: View) {
        toast("We're not blocking!")
    }

    private fun doSlowStuff() {
        while (true) {
            Thread.sleep(1000)
            contents.set(contents.get() + 1)
        }
    }

    fun onClicked(view: View) = onClicked()
}

object TextViewBinding {

    @JvmStatic
    @BindingAdapter("bind:numberText")
    fun bindNumberText(textView: TextView, number: Int) {
        textView.text = "" + number
    }
}

