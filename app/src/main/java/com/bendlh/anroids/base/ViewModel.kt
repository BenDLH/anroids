package com.bendlh.anroids.base

import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.View

/**
 *
 */
interface ViewModel {
    fun inflateView(inflater: LayoutInflater): View
    fun getBinding(): ViewDataBinding?
    fun onStart()
    fun onStop()
    fun onBackPressed(): Boolean
}