package com.bendlh.anroids.base

import android.content.Intent
import android.os.Bundle
import android.support.annotation.IntDef
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import com.bendlh.anroids.R

/**
 *
 */
abstract class BaseActivity: AppCompatActivity() {

    protected abstract val viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleIntent(intent)
        setContentView(viewModel.inflateView(LayoutInflater.from(this)))
    }

    protected abstract fun handleIntent(intent: Intent)

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onBackPressed() {
        if (!viewModel.onBackPressed()) finishActivity()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    @JvmOverloads
    protected fun launchActivity(intent: Intent, @Transition animation: Int = IN_FROM_RIGHT) {
        startActivity(intent)
        doTransitionAnimation(animation)
    }

    protected fun launchActivityForResult(intent: Intent, requestCode: Int) {
        startActivityForResult(intent, requestCode)
        doTransitionAnimation(IN_FROM_RIGHT)
    }

    open fun finishActivity() {
        finishActivityWithAnimation(OUT_RIGHT)
    }

    protected fun finishActivityWithAnimation(@Transition transitionAnimation: Int) {
        finish()
        doTransitionAnimation(transitionAnimation)
    }

    private fun doTransitionAnimation(@Transition animation: Int) {
        when (animation) {
            IN_FROM_BOTTOM -> overridePendingTransition(R.anim.in_from_bottom, R.anim.fade_out)
            IN_FROM_RIGHT -> overridePendingTransition(R.anim.in_from_right, R.anim.fade_out)
            IN_FADE -> overridePendingTransition(R.anim.fade_in, R.anim.wait)
            OUT_BOTTOM -> overridePendingTransition(R.anim.fade_in, R.anim.out_bottom)
            OUT_RIGHT -> overridePendingTransition(R.anim.fade_in, R.anim.out_right)
            OUT_FADE -> overridePendingTransition(R.anim.wait, R.anim.fade_out)
        }
    }

    // endregion

    companion object {

        // Used for Fragment and Activity transitions
        @IntDef(IN_FROM_BOTTOM.toLong(), IN_FROM_RIGHT.toLong(), OUT_BOTTOM.toLong(), OUT_RIGHT.toLong())
        annotation class Transition

        const val IN_FROM_BOTTOM = 0
        const val IN_FROM_RIGHT = 1
        const val IN_FADE = 2
        const val OUT_BOTTOM = 3
        const val OUT_RIGHT = 4
        const val OUT_FADE = 5
    }
}