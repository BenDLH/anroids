package com.bendlh.anroids

import android.app.Application

/**
 *
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        app = this
    }

    companion object {
        private lateinit var app: App

        fun get() = app
    }
}