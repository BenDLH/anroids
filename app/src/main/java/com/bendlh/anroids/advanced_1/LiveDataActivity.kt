package com.bendlh.anroids.advanced_1

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import co.metalab.asyncawait.async
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel

/**
 * What is LiveData??
 *
 * LiveData is a container around a value, providing an API to observe any changes made upon it.
 * The use of LiveData comes from providing a LifeCycleOwner (eg. Activity) that the LiveData can query.
 * With a LifeCycleOwner to query, the LiveData can ensure the observer is only called between "active" lifecycle calls
 * (ie. between onStart() and onStop()). They also ensure the LifeCycleOwner is released to avoid leaks.
 *
 * The LiveData class has an extended child, MutableLiveData. MutableLiveData exposes a setter for the value.
 * Instantiate MutableLiveData objects in your data layer where the value will be updated, and expose them only as LiveData
 * where the data is consumed.
 */
class LiveDataActivity : BaseActivity() {

    override val viewModel = LiveViewModel(this, LiveDataDataProvider())

    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, LiveDataActivity::class.java)
    }
}

// But here we express it as only LiveData, to ensure only the creator updates the contents
class LiveViewModel(lifecycleOwner: LifecycleOwner, dataProvider: LiveDataDataProvider) : BaseViewModel() {

    val liveString = ObservableField<String>()

    init {
        // We observe on the LiveData (providing a lifecycle owner), and create an observer that will update this observable field
        dataProvider.liveString.observe(lifecycleOwner, Observer { text ->
            liveString.set(text)
        })
    }

    override val bindingLayoutRes = R.layout.view_live
}

class LiveDataDataProvider {

    val liveString = MutableLiveData<String>().apply {
        value = "This is LiveData"
    }

    init {

        var times = 0
        async {

            while (true) {
                Thread.sleep(1000)
                times++
                liveString.value = "This is LiveData, updated: $times times"
            }
        }
    }
}