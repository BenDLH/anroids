package com.bendlh.anroids.basic.basic_2

import android.content.Context
import android.content.Intent
import android.databinding.ObservableArrayList
import android.view.View
import com.bendlh.anroids.BR
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.basic.basic_3.DetailsActivity
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 * Just a basic activity extending our base class
 */
class ListActivity: BaseActivity() {

    override val viewModel = ListViewModel {
        launchActivity(DetailsActivity.getIntent(this, it))
    }

    override fun handleIntent(intent: Intent) {}

    // Kotlin doesn't have static, but we do have companion objects, which are inner singleton classes
    companion object {
        fun getIntent(context: Context) = Intent(context, ListActivity::class.java)
    }
}

class ListViewModel(onMessageClicked: (String) -> Unit): BaseViewModel() {

    /**
     * This class is part of an awesome library, BindingCollectionAdapter, that handles creating
     * adapters from items and an "item binding" (not just for RecyclerViews either).
     */
    val itemBinding /*: ItemBinding<ListItemViewModel>*/ = ItemBinding.of<ListItemViewModel>(BR.viewModel, R.layout.item_list)

    /**
     *  We use an ObservableArrayList here, which allows us to update the list and have the layout
     *  update trigger when the contents change.
     */
    val items/*: ObservableArrayList<ListItemViewModel>*/ = ObservableArrayList<ListItemViewModel>().apply {

        // Just adding an item for each number in a range from 0 to 20
        addAll((0..20).map {

            // Create the list item
            ListItemViewModel("Item $it", onMessageClicked)
        })
    }

    override val bindingLayoutRes = R.layout.view_list
}

/**
 * This is our item view model, it represents each item in the list.
 *
 * In Kotlin this is all you need for a basic class with a constructor taking a String.
 * We're also creating a field for the String in the constructor declaration
 */
class ListItemViewModel(val text: String, private val _onMessageClicked: (String) -> Unit) {
    fun onMessageClicked(view: View) = _onMessageClicked(text)
}