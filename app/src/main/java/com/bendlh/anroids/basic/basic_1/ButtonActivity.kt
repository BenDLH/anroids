package com.bendlh.anroids.basic.basic_1

import android.content.Context
import android.content.Intent
import android.view.View
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.basic.basic_2.ListActivity
import com.bendlh.anroids.base.BaseViewModel


/**
 * Just a basic activity extending our base class
 */
class ButtonActivity : BaseActivity() {

    /**
     * The base Activity class will take this viewModel and inflate it as it's layout,
     * then call it's lifecycle methods.
     *
     * Kotlin doesn't just let you override functions, but fields as well!
     */
    override val viewModel = ButtonViewModel {
        launchActivity(ListActivity.getIntent(this))
    }

    // We don't care about this here
    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, ButtonActivity::class.java)
    }
}

class ButtonViewModel(private val _onButtonClicked: () -> Unit): BaseViewModel() {

    /**
     * This layout resource will be inflated, and this viewModel will be injected into the layout for DataBinding
     *
     * Yay, more overridden fields!
     */
    override val bindingLayoutRes = R.layout.view_main

    fun onButtonClicked(view: View) = _onButtonClicked()
}