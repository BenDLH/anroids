package com.bendlh.anroids.basic.basic_3

import android.content.Context
import android.content.Intent
import android.databinding.BindingAdapter
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.base.ViewModel
import com.bendlh.anroids.basic.basic_4.ExpandableListActivity

/**
 * Details Activity
 */
class DetailsActivity: BaseActivity() {

    override val viewModel by lazy {
        DetailsViewModel(_message) {
            launchActivity(ExpandableListActivity.getIntent(this))
        }
    }

    private lateinit var _message: String

    override fun handleIntent(intent: Intent) {
        _message = intent.getStringExtra(EXTRA_MESSAGE)
    }

    companion object {

        const val EXTRA_MESSAGE = "EXTRA_MESSAGE"

        fun getIntent(context: Context, text: String): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(EXTRA_MESSAGE, text)
            return intent
        }
    }
}

/**
 * Page ViewModel
 */
class DetailsViewModel(val message: String, onChildClicked: () -> Unit) : BaseViewModel() {

    val child = DetailsChildViewModel("Child message", onChildClicked)

    override val bindingLayoutRes = R.layout.view_details
}

/**
 * Nested/Child ViewModel
 */
class DetailsChildViewModel(val message: String, private val _onClicked: () -> Unit) : BaseViewModel() {

    override val bindingLayoutRes = R.layout.view_details_child

    fun onClicked(view: View) = _onClicked()
}

/**
 * Static binding class used by our XML layouts
 */
object FrameLayoutBindings {

    @JvmStatic
    @BindingAdapter("bind:viewModel")
    fun bindViewModel(frameLayout: FrameLayout, viewModel: ViewModel) {
        frameLayout.removeAllViews()
        frameLayout.addView(viewModel.getBinding()?.root ?: viewModel.inflateView(LayoutInflater.from(frameLayout.context)))
    }
}