package com.bendlh.anroids.basic.basic_4

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bendlh.anroids.BR
import com.bendlh.anroids.R
import com.bendlh.anroids.coroutines.coroutines_1.NonBlockingActivity
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 * Expandable List Activity
 */
class ExpandableListActivity: BaseActivity() {

    override val viewModel = ExpandableListViewModel {
        launchActivity(NonBlockingActivity.getIntent(this))
    }

    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, ExpandableListActivity::class.java)
    }
}

/**
 * Page ViewModel
 */
class ExpandableListViewModel(private val _onNextClicked: () -> Unit): BaseViewModel() {

    val itemBinding = ItemBinding.of<ExpandableListItemViewModel>(BR.viewModel, R.layout.item_expandable_list)
    val items = ObservableArrayList<ExpandableListItemViewModel>().apply {
        addAll((0..20).map {
            ExpandableListItemViewModel()
        })
    }

    fun onNextClicked(view: View) = _onNextClicked()

    override val bindingLayoutRes = R.layout.view_expandable_list
}

/**
 * Expandable list item
 */
class ExpandableListItemViewModel {

    val expanded = ObservableBoolean()
    val expandIconRotation = ObservableInt()

    fun onToggleClicked(view: View) {
        expanded.set(expanded.get().not())
        expandIconRotation.set(if (expanded.get()) 180 else 0)
    }
}

/**
 * Static binding class used by our XML layouts
 */
object TextViewBindings {

    class ExpandData(
            var expanded: Boolean,
            val originalHeight: Int,
            val animator: ValueAnimator
    )

    @JvmStatic
    @BindingAdapter("bind:expanded")
    fun bindExpanded(textView: TextView, expand: Boolean) {
        if ((textView.tag as? ExpandData)?.expanded == true == expand) return

        textView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.makeMeasureSpec((textView.parent as ViewGroup).height, View.MeasureSpec.UNSPECIFIED))
        if (textView.tag == null) textView.tag = ExpandData(expand, textView.measuredHeight, ObjectAnimator.ofInt(0, 1))

        val data = (textView.tag as ExpandData)
        val endHeight = if (expand) Math.max(data.originalHeight, textView.height) else Math.min(data.originalHeight, textView.height)
        val animator = data.animator

        animator.removeAllListeners()
        animator.cancel()
        animator.addUpdateListener({ animation ->

            if (expand && textView.height > animation.animatedFraction * endHeight) return@addUpdateListener
            if (expand.not() && textView.height < animation.animatedFraction * endHeight) return@addUpdateListener

            textView.layoutParams.height = (animation.animatedFraction * endHeight).toInt()
            textView.requestLayout()

            if (expand && animation.animatedFraction == 0f) textView.visibility = View.VISIBLE
        })
        animator.duration = 500

        if (expand) {
            animator.start()
        } else {
            animator.reverse()
        }

        data.expanded = expand
    }
}