package com.bendlh.anroids.basic.retrofit

import android.content.Context
import android.content.Intent
import android.databinding.ObservableField
import com.bendlh.anroids.R
import com.bendlh.anroids.base.BaseActivity
import com.bendlh.anroids.base.BaseViewModel
import com.bendlh.anroids.utils.Ticker
import com.bendlh.anroids.utils.VantageServiceManager
import com.bendlh.anroids.utils.log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *
 */
class RetrofitActivity : BaseActivity() {

    override val viewModel = RetrofitViewModel(RetrofitDataProvider())

    override fun handleIntent(intent: Intent) {}

    companion object {
        fun getIntent(context: Context) = Intent(context, RetrofitActivity::class.java)
    }
}

class RetrofitViewModel(private val dataProvider: RetrofitDataProvider): BaseViewModel() {

    val tickerPrice = ObservableField<String>()
    val tickerVolume = ObservableField<String>()
    val tickerTime = ObservableField<String>()

    override val bindingLayoutRes = R.layout.view_retrofit

    override fun onStart() {
        super.onStart()

        dataProvider.subscribeToChanges(this::onProductTickerUpdated)
    }

    override fun onStop() {
        super.onStop()

        dataProvider.unsubscribeFromChanges()
    }

    private fun onProductTickerUpdated(productTicker: Ticker) {
        tickerPrice.set("" + productTicker.price)
        tickerVolume.set("" + productTicker.volume)
        tickerTime.set("" + productTicker.time)
    }
}

class RetrofitDataProvider {

    private var onProductTickerUpdated: (Ticker) -> Unit = {}

    private val vantageService = VantageServiceManager.service

    fun subscribeToChanges(onProductTickerUpdated: (productTicker: Ticker) -> Unit) {
        this.onProductTickerUpdated = onProductTickerUpdated

        vantageService.getProductTicker("BCH-BTC").enqueue(object: Callback<Ticker> {
            override fun onFailure(call: Call<Ticker>?, t: Throwable?) {
                log("Error: " + t)
            }

            override fun onResponse(call: Call<Ticker>?, response: Response<Ticker>?) {

                if (response?.isSuccessful == true) {
                    onProductTickerUpdated(response.body())
                } else {
                    log("Error: " + response?.code())
                }
            }
        })
    }

    fun unsubscribeFromChanges() {
        onProductTickerUpdated = {}
    }
}
