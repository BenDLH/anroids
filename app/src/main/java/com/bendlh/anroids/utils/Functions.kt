package com.bendlh.anroids.utils

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.*
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.bendlh.anroids.App

/**
 *
 */
fun toast(message: String) {
    Toast.makeText(App.get(), message, Toast.LENGTH_SHORT).show()
}

/*fun dateTime(year: Int = 0, monthOfYear: Int = 0, dayOfMonth: Int = 0, hourOfDay: Int = 0, minuteOfHour: Int = 0, secondOfMinute: Int = 0, millisOfSecond: Int = 0): DateTime {
    return DateTime.now()
            .withDate(year, monthOfYear, dayOfMonth)
            .withTime(hourOfDay, minuteOfHour, secondOfMinute, millisOfSecond)
}

fun openInCustomTabs(context: Context, url: String, startEnterAnim: Int = 0, startExitAnim: Int = 0, exitEnterAnim: Int = 0, exitExitAnim: Int = 0) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

        CustomTabsIntent.Builder()
                .setToolbarColor(getColor(R.color.white))
                .setStartAnimations(context, startEnterAnim, startExitAnim)
                .setExitAnimations(context, exitEnterAnim, exitExitAnim)
                .build()
                .launchUrl(context, Uri.parse(url))
    } else {

        context.startActivity(
                Intent().setAction(Intent.ACTION_VIEW)
                        .setData(Uri.parse(url))
        )
    }
}*/

fun getString(@StringRes resId: Int): String = App.get().getString(resId)!!

fun getString(@StringRes resId: Int, vararg formatArgs: Any): String = App.get().resources.getString(resId, *formatArgs)

fun getPluralString(@PluralsRes resId: Int, quantity: Int): String = App.get().resources.getQuantityString(resId, quantity)

fun getPluralString(@PluralsRes resId: Int, quantity: Int, vararg formatArgs: Any): String = App.get().resources.getQuantityString(resId, quantity, *formatArgs)

//fun getColor(@ColorRes resId: Int) = ContextCompat.getColor(App.get(), resId)

fun getDimen(@DimenRes resId: Int) = App.get().resources.getDimension(resId)

fun getDrawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(App.get(), resId)!!

fun getPreferences(name: String): SharedPreferences = App.get().getSharedPreferences(name, Context.MODE_PRIVATE)

fun Any.log(message: String) {
    Log.d(this.toString(), message)
}

