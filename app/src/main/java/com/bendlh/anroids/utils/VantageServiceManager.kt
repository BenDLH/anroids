package com.bendlh.anroids.utils

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object VantageServiceManager {

    val service: VantageService

    init {
        val clientBuilder = OkHttpClient.Builder()

        val client = clientBuilder.build()

        val retrofit = retrofit2.Retrofit.Builder()
                .baseUrl("https://api.gdax.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()

        service = retrofit.create(VantageService::class.java)
    }
}

interface VantageService {

    @GET("products/{product-id}/ticker")
    fun getProductTicker(@Path("product-id") productId: String): Call<Ticker>
}

data class Ticker (
        val trade_id: Int,
        val price: Float,
        val size: String,
        val bid: Float,
        val ask: Float,
        val volume: String,
        val time: String
)